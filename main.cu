#include <iostream>
#include <fstream>
#include <vector>

#define n 3 // длина одномерной цепочки,
            // бетта и обменный интеграл полагается = 1.
#define J 1
#define h 1

struct chain_type
{
    short S[n]{};
    short E=0;
    short M=0;
};

__global__ void ThreadIit(chain_type *chain, unsigned int length)
{
    unsigned int x = threadIdx.x + blockIdx.x*blockDim.x;
    if (x<length)
    {
        unsigned int bit = x;
        for(int i=0; i<n; i++)
        {
            chain[x].S[i] = bit & 1 ? 1 : -1;
            bit >>= 1;
            chain[x].M += chain[x].S[i];
        }
        for(int i=0; i<n; i++)
        {
            chain[x].E += -J*chain[x].S[i]*chain[x].S[(i+1)%n];
        }
        //chain[x].E += h*chain[x].M;
    }
}

__global__ void IterationMatrix(int *Z, chain_type *chain, unsigned int length)
{
    int thread_No_x = threadIdx.x + blockIdx.x*blockDim.x;
    int thread_No_y = threadIdx.y + blockIdx.y*blockDim.y;
    if(thread_No_x<length && thread_No_y<length)
    {
        /*printf("threadNo_x %d, _y %d: %d %d %d    %d %d %d \n", thread_No_x, thread_No_y,
               chain[thread_No_x].S[0], chain[thread_No_x].S[1], chain[thread_No_x].S[2],
               chain[thread_No_y].S[0], chain[thread_No_y].S[1], chain[thread_No_y].S[2]);*/
        for(int i=0; i<n; i++)
        {
            Z[thread_No_x+thread_No_y*length] += -J*chain[thread_No_x].S[i]*chain[thread_No_y].S[i];
        }
    }
}

int main()
{
    unsigned int length = 2<<(n-1);
    std::vector<chain_type> chain(length);
    std::vector<int> Z(2<<(2*n-1));
    int *dev_Z;
    chain_type *dev_chain;
    dim3 blocks(256, 256);
    dim3 threads(32, 32);
    cudaMalloc((void**)&dev_Z, (2<<(2*n-1))*sizeof (int));
    cudaMalloc((void**)&dev_chain, length*sizeof (chain_type));
    cudaMemcpy(dev_Z, Z.data(), (2<<(2*n-1))*sizeof (int), cudaMemcpyHostToDevice);
    cudaMemcpy(dev_chain, chain.data(), length*sizeof (chain_type), cudaMemcpyHostToDevice);

    ThreadIit<<<65535,1024>>>(dev_chain, length);
    IterationMatrix<<<blocks,threads>>>(dev_Z, dev_chain, length);

    cudaMemcpy(Z.data(), dev_Z, (2<<(2*n-1))*sizeof (int), cudaMemcpyDeviceToHost);
    cudaMemcpy(chain.data(), dev_chain, length*sizeof (chain_type), cudaMemcpyDeviceToHost);
    cudaFree(dev_Z);
    cudaFree(dev_chain);

    for (int i=0; i<length; i++)
    {
        for (int j=0; j<length; j++)
        {std::cout << Z[j+i*length] << "B ";}
        std::cout << "\n";
    }
    std::cout << "\n" << "Thread:" << "\n";
    for (int i=0; i<n; i++)
    {
        for(int j=0; j<length; j++)
        {std::cout << chain[j].S[i] << "     ";}
        std::cout << "\n";
    }
    std::cout << "Thread Energy:" << "\n";
    for (int i=0; i<length; i++)
    {
        std::cout << chain[i].E << "     ";
    }
    std::cout << "\n" << "Thread spin excess:" << "\n";
    for (int i=0; i<length; i++)
    {
        std::cout << chain[i].M << "     ";
    }
    std::ofstream file("matrix.txt");
    for (int i=0; i<length; i++)
    {
        for (int j=0; j<length; j++)
        {file << Z[j+i*length] << "B ";}
        file << "\n";
    }
    file << "\n" << "Thread:" << "\n";
    for (int i=0; i<n; i++)
    {
        for(int j=0; j<length; j++)
        {file << chain[j].S[i] << "     ";}
        file << "\n";
    }
    file << "\n" << "Thread Energy:" << "\n";
    for (int i=0; i<length; i++)
    {
        file << chain[i].E << "     ";
    }
    file << "\n" << "Thread spin excess:" << "\n";
    for (int i=0; i<length; i++)
    {
        file << chain[i].M << "     ";
    }
}